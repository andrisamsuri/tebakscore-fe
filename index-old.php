<?php include('inc_header.php'); ?>
<!-- middle -->
<section class="banner">
    <div class="logo">  
        <a href="">
            <img src="images/material/logo.png" alt="megajiwa tebak score" /> 
        </a>        
    </div>
    <div class="text">
        <i>Periode 14 Juni - 15 July 2018</i>
        <h2>
            PIALA DUNIA<br/>
            MEGA JIWA
        </h2>
        <h3>Dapatkan hadiah menarik 
            untuk <b>150</b> orang <b>beruntung!</b></h3>
        <div class="socmed">
            <a href=""><img src="images/material/fb-ico.png" alt="" /></a>
            <a href=""><img src="images/material/tw-ico.png" alt="" /></a>
            <a href=""><img src="images/material/yt-ico.png" alt="" /></a>
        </div>
    </div>
</section> 
<section class="fight-today">
    <div class="wrapper">
        <h3><b>FIFA WORLD CUP.</b> Apakah tim kamu siap bertanding?</h3>
        <div class="box-blue">
            <div class="title">
                <h3><b>FIFA WORLD CUP.</b> - GROUP A</h3>
                <span>Juni 14</span>
            </div>
            <?php
            for ($i = 0; $i < 3; $i++) {
                ?>
                <div class="row">
                    <div class="input-text">
                        <input type="text" value="0">
                        <a href="" class="plus" ></a>
                        <a href="" class="min" ></a>
                    </div>
                    <div class="vs <?= ($i % 2 == 0 ? "red" : "" ) ?>">
                        <div class="bendera left"> 
                            <img src="images/material/bendera.png" alt="bendera" /> 
                        </div>
                        <div class="text">
                            <div class="left">Uruguay</div>
                            <div class="center">VS</div>
                            <div class="right">Egypt</div>
                        </div>                    
                        <div class="bendera right"> 
                            <img src="images/material/bendera-1.png" alt="bendera" /> 
                        </div>
                        <div class="time">
                            17:00 WIB
                        </div>
                    </div>
                    <div class="input-text">
                        <input type="text" value="0">
                        <a href="" class="plus" ></a>
                        <a href="" class="min" ></a>
                    </div>
                    <a href="" class="button">tebak</a>
                </div>
                <?php
            }
            ?>
        </div>

    </div>
</section>

<section class="fight-last">
    <div class="wrapper">
        <h3><b>CEK PERTANDINGANYA.</b> Apakah tim kamu sudah bertanding?</h3>
        <div class="after_clear">
            <div class="box">
                <h3>PERMAINAN SEBELUMNYA.</h3>  
                <?php
                for ($i = 0; $i < 4; $i++) {
                    ?>
                    <div class="row">
                        <div class="country left">                           
                            <div class="img"><img src="images/material/bendera.png" alt="bendera"/></div>
                            <span>Russia</span>
                        </div>
                        <div class="res">
                            <small>4June / 17:00 WIB</small>
                            2 - 0
                        </div>
                        <div class="country right">
                            <div class="img"><img src="images/material/bendera-1.png" alt="bendera"/></div>
                            <span>Saudi Arabia</span>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="box">
                <h3>PERTANDINGAN SELANJUTNYA..</h3>  
                <?php
                for ($i = 0; $i < 4; $i++) {
                    ?>
                    <div class="row">
                        <div class="country left">
                            <div class="img"><img src="images/material/bendera.png" alt="bendera"/></div>
                            <span>Russia</span>
                        </div>
                        <div class="res">
                            <small>4June / 17:00 WIB</small>
                            VS
                        </div>
                        <div class="country right">
                            <div class="img"><img src="images/material/bendera-1.png" alt="bendera"/></div>
                            <span>Saudi Arabia</span>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</section>

<!-- end of middle -->
<?php include('inc_footer.php'); ?>