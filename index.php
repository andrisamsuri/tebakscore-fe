<?php include('inc_header.php'); ?>
<!-- middle -->
<section class="banner">
    <div class="logo">  
        <a href="">
            <img src="images/material/logo.png" alt="megajiwa tebak score" /> 
        </a>        
    </div>
    <div class="text">
        <i>Periode 14 Juni - 15 July 2018</i>
        <h2>
            PIALA DUNIA<br/>
            MEGA JIWA
        </h2>
        <h3>Dapatkan hadiah menarik 
            untuk <b>150</b> orang <b>beruntung!</b></h3>
        <div class="socmed">
            <a href=""><img src="images/material/fb-ico.png" alt="" /></a>
            <a href=""><img src="images/material/tw-ico.png" alt="" /></a>
            <a href=""><img src="images/material/yt-ico.png" alt="" /></a>
        </div>
    </div>
</section> 
<section class="col-3">
    <div class="col">
        <img src="images/material/small-banner-1.jpg" alt="" />
    </div>
    <div class="col">
        <img src="images/material/small-banner-2.jpg" alt="" />
        <div class="countdown" style="background: rgba(245,124,32,0.9);" data-countdown = "06 14 2018 00:00:00">
            <div class="wrap">
                <div class="box">
                    <span class="hours">18</span>
                    <small>Hours</small>
                </div>
                <div class="box">
                    <span class="min">26</span>
                    <small>minutes</small>
                </div>
                <div class="box">
                    <span class="sec">54</span>
                    <small>second</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <img src="images/material/small-banner-3.jpg" alt="" />
        <div class="countdown" style="background: rgba(255,203,8,0.9);" data-countdown = "06 11 2018 00:00:00">
            <div class="wrap">
                <div class="box">
                    <span class="hours">18</span>
                    <small>Hours</small>
                </div>
                <div class="box">
                    <span class="min">26</span>
                    <small>minutes</small>
                </div>
                <div class="box">
                    <span class="sec">54</span>
                    <small>second</small>
                </div>
            </div>
        </div>

    </div>
</section>
<section class="fav-sec"> 
    <div class="wrapper">
        <div class="title">
            <h2>SIAPA TIM FAVORITMU?</h2>
            <h5>Pilih tim kesayangan kamu di piala dunia!</h5>
        </div>
        <div class="flag-slider"> 
            <?php
            for ($i = 0; $i < 15; $i++) {
                ?>
                <div class="slide">
                    <label>
                        <input type="radio" name="country" /><img src="images/material/bendera.png" alt="bendera" />
                        <span>Name</span>
                    </label>
                    <label>
                        <input type="radio" name="country" /><img src="images/material/bendera-1.png" alt="bendera" />
                        <span>Name</span>
                    </label>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="box-form">
            <div class="row">
                <label>Berikan alasanmu!</label>
                <textarea placeholder="Type here"></textarea>
            </div>
            <input type="submit" value="kirim" class="button" />
        </div>
    </div>
</section>

<script>
    $(document).ready(function () {

        $(".flag-slider input[type=radio]").change(function () {
            if ($(this).is(":checked")) {
                $(".flag-slider .active").removeClass('active');
                $(this).parent().addClass('active');
            }
        });

        $(".countdown").each(function () {
            var datena = $(this).attr('data-countdown');
            var elem  = $(this);
            var countDownDate = new Date(datena).getTime();

            // Update the count down every 1 second
            var x = setInterval(function () {

                // Get todays date and time
                var now = new Date().getTime();

                // Find the distance between now an the count down date
                var distance = countDownDate - now;

                // Time calculations for days, hours, minutes and seconds
//                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
//                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 ) );
                var hours = Math.floor((distance / (1000 * 60 * 60)) );
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Display the result in the element with id="demo"
                //document.getElementById("demo").innerHTML = days + "d " + hours + "h "
                //        + minutes + "m " + seconds + "s ";
                
                elem.find('.hours').html(hours);
                elem.find('.min').html(minutes);
                elem.find('.sec').html(seconds);

                // If the count down is finished, write some text 
            }, 1000);
        })
    })
</script>
<!-- end of middle -->
<?php include('inc_footer.php'); ?>